# Xperia XZ2 Bootloader Firmware
This repository contain script and proprietary bootloader firmware to flash Xperia XZ2 S1 proprietary bootloader firmware in recovery mode (without Flash mode)

# Purpose
- Upgrade latest bootloader firmware to boot Android 10+ custom ROMs
- Who have Xperia XZ2 with Oreo/Pie bootloader want update to latest Q bootloader without flash mode
- Reflash/refresh bootloader partition
- Fix fastboot mode issue on X_BOOT_SDM845_LA2_0_1_Q_206 (52.1.A.0.618) Q Bootloader
- Fix fastboot mode issue WITH USB 3.0 ports on X_BOOT_SDM845_LA2_0_1_Q_207 (52.1.A.3.49) Q Bootloader

# Bootloader Information 
XBoot (S1) version : 

`1310_7079_X_BOOT_SDM845_LA2.0.1_Q_208`

Bootloader Android version : 

`Android 10`

Supported HW Config Revision :

`HWC_Tama_Com_001 - COMMERCIAL_0008B0E1`

`HWC_Tama_Dev_001 - DEVELOPMENT`

# WARNING !
Before flashing bootloader, please make sure flash correct bootloader with correct specific HW Config Revision above! If not, your device will can't verify bootloader signature and goes hard brick state!

To determine what HW Config Revision your device using, there are two way to check :

**Method 1 (device with working Android OS and running stock fw) :**
- Open dialer and type `*#*#7378423#*#*`, Service Menu will open.
- Go to Service Info -> Configuration or Security
- Look at `HW Config rev`
- If it is `HWC_Tama_Com_001`, flash bootloader with `HWC_Tama_Com_001` variant
- If it is `HWC_Tama_Dev_001`, flash bootloader with `HWC_Tama_Dev_001` variant

**Method 2 (determine from flash mode) :**

Linux (Debain based) :
- Put your device to proprietary flash mode and connect with PC using USB cable
- Open terminal and type `sudo apt install -y fastboot` to install fastboot
- After installed fastboot, type `fastboot getvar Security-state`
- If `security-state` value is `U0vSz8eM1gk1FZup2A6hQlhcZnS9ZR3Up1pg2aPJewY=`, flash bootloader with `HWC_Tama_Com_001` variant
- If `security-state` value is `wSaq4guKAposHQYWumdHOVvrDFlLAbDib7JISLENTVc=`, flash bootloader with `HWC_Tama_Dev_001` variant

Linux (Arch Linux based) :
- Put your device to proprietary flash mode and connect with PC using USB cable
- Open terminal and type `sudo pacman -S android-tools` to install fastboot
- After installed fastboot, type `fastboot getvar Security-state`
- If `security-state` value is `U0vSz8eM1gk1FZup2A6hQlhcZnS9ZR3Up1pg2aPJewY=`, flash bootloader with `HWC_Tama_Com_001` variant
- If `security-state` value is `wSaq4guKAposHQYWumdHOVvrDFlLAbDib7JISLENTVc=`, flash bootloader with `HWC_Tama_Dev_001` variant

Windows :
- Put your device to proprietary flash mode and connect with PC using USB cable
- Download and install Sony Xperia driver from here : [Windows 32bit](https://github.com/cuynu/oldarchive/releases/download/23.3.4/Sony_Mobile_Software_Update_Drivers_x86_Setup.msi) | [Windows 64bit](https://github.com/cuynu/oldarchive/releases/download/23.3.4/Sony_Mobile_Software_Update_Drivers_x64_Setup.msi)
- After installed driver, [download newflasher](https://forum.xda-developers.com/attachments/newflasher_v57-zip.5948007/) and extract then open `newflasher.exe`
- Press `F` then `N`, its will read all info of device in proprietary flash mode and reboot device to fastboot mode 
- Scroll up and look the `Security state`
- If `Security state` value is `U0vSz8eM1gk1FZup2A6hQlhcZnS9ZR3Up1pg2aPJewY=`, flash bootloader with `HWC_Tama_Com_001` variant
- If `Security-state` value is `wSaq4guKAposHQYWumdHOVvrDFlLAbDib7JISLENTVc=`, flash bootloader with `HWC_Tama_Dev_001` variant

# How to Flash
- Boot into any custom recovery available for Xperia XZ2 such as TWRP,OrangeFox and copy bootloader flashable zip to storage then flash using install zip/sideload function (TWRP,OrangeFox).
- For LineageOS Recovery, use sideload function.

# Why not flash bootloader in fastboot?
- Fastboot mode in Sony Xperia devices are have limitations, only `boot` `dtbo` `system` `userdata` `vendor` `dtbo` `oem` are allowed to flash even with unlocked bootloader state, so only proprietary flash mode, recovery mode and Android shell can flash to bootloader partition as they not protected like fastboot mode.

# Rescue from hard brick
**NOTE : This only works if you are mistakenly erase or broken "xbl_a" or "xbl_b" partition. If you erased other bootloader partition or Trim Aera (TA) partition, then you NEED to reflash bootloader partition with physical access.**

SIGN of XBL is corrupt : Device does not response or turn on at all, even the LED lights, then recognize on PC as specific EDL mode after a few seconds.

- Install the EDL tool by bkerler : [bkerler/edl](https://github.com/bkerler/edl#linux-debianubuntumintetc)
- Download the proper XBL file for your variant : [COMMERCIAL_0008B0E1](https://gitlab.com/cuynu/akari-bootloader-sdm845-q/-/raw/X_BOOT_SDM845_LA2_0_1_Q_208/HWC_Tama_Com_001/images/xbl_X_BOOT_SDM845_LA2_0_1_Q_208.mbn?ref_type=heads) | [DEVELOPMENT](https://gitlab.com/cuynu/akari-bootloader-sdm845-q/-/raw/X_BOOT_SDM845_LA2_0_1_Q_208/HWC_Tama_Dev_001/images/xbl_208.mbn?ref_type=heads)
- If you don't remember what variant your device is, just try both :)
- Connect the device in hard brick state, make sure it is detected as `Bus 000 Device 000: ID 0fce:ade5 Sony Ericsson Mobile Communications AB QUSB_BULK_CID:0402_SN:00000000`
- Run this command (for device with COMMERCIAL variant)
```
edl --loader=xbl_X_BOOT_SDM845_LA2_0_1_Q_208.mbn
```
- For DEVELOPMENT variant 
```
edl --loader=xbl_208.mbn
```
- Output must  be like this 
```
.........main - Device detected :)
sahara - Protocol version: 2, Version supported: 1
main - Mode detected: sahara
sahara - 
Version 0x2
------------------------
HWID:              0x0008b0e100010004 (MSM_ID:0x0008b0e1,OEM_ID:0x0001,MODEL_ID:0x0004)
CPU detected:      "SDM845"
PK_HASH:           0xc8d92a2dba1482588eff712963329500ac7155cea8494fdd4b7b180f2f871801
Serial:            0x23144207

sahara - Protocol version: 2, Version supported: 1
sahara - Uploading loader xbl_X_BOOT_SDM845_LA2_0_1_Q_208.mbn ...
sahara - 64-Bit mode detected.
sahara - Firehose mode detected, uploading...
sahara - Loader successfully uploaded.
```
Device should boot xbl and start loads other bootloader components and then abl. After device booted to xbl stage, immediately enter fastboot, reflash xbl partition with the file that has been successfully loaded on EDL stage.

# DISCLAIMER 
Xperia XZ2 bootloader binary are proprietary and protected with RSA2048 signature by Sony Mobile Communications Inc and Qualcomm Incorporated. Source code are not available and bootloader binary cannot be modified to flash into device because RSA2048 signature key will change in modified binary so device will reject modified bootloader binary with Secure boot check on Primary Bootloader (PBL) burned to Snapdragon CPU.

If you want or need to modify bootloader binaries and want to flash it, you must need to change the original SDM845 CPU from Xperia XZ2 mainboard to other or generic one that come with NO SECURE BOOT  (If PBL able to load generic or any unsigned oem SDM845 firehose loaders, then CPU are SECURE BOOT NO) or WITH SECURE BOOT (if you have its PBL BootROM RSA2048 signature)
